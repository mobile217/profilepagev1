import 'package:flutter/material.dart';
enum APP_THEME{LIGHT, DARK}
void main() {
  runApp(ContacctProfilePage());
}

class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.orange,
            iconTheme: IconThemeData(
              color: Colors.orange.shade50,
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.orange
        )
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.orange,
            iconTheme: IconThemeData(
              color: Colors.orange.shade50,
            )
        ),
        iconTheme: IconThemeData(
            color: Colors.orange
        )
    );
  }
}

class ContacctProfilePage extends StatefulWidget{
  @override
  State<ContacctProfilePage> createState() => _ContacctProfilePageState();
}

class _ContacctProfilePageState extends State<ContacctProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
        ? MyAppTheme.appThemeLight()
        : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.pets,color: Colors.white),backgroundColor: Colors.orange,
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}


Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call,color: Colors.orange),
    title: Text("081-940-3756"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon:Icon(Icons.message),
      color: Colors.orange,
      onPressed: (){},
    ),
  );
}

Widget otherPhoneListTile(){
  return ListTile(
    leading: Icon(null),
    title: Text("063-996-4049"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon:Icon(Icons.message),
      color: Colors.orange,
      onPressed: (){},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email,color: Colors.orange),
    title: Text("63160217@go.buu.ac.th"),
    subtitle: Text("work"),
    // trailing: IconButton(
    //   icon:Icon(null),
    //   color: Colors.indigo.shade800,
    //   onPressed: (){},
    // ),
  );
}

Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on,color: Colors.orange),
    title: Text("32/3 Moo 2, Song Salung Subdistrict, Klaeng District, Rayong Province 21110"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon:Icon(Icons.directions),
      color: Colors.orange,
      onPressed: (){},
    ),
  );
}

AppBar buildAppBarWidget(){
  return AppBar(
    // backgroundColor: Colors.orange,
    leading: Icon(Icons.arrow_back,color: Colors.black,
    ),
    actions: <Widget>[
      IconButton(
          icon: Icon(Icons.star_border),
          color: Colors.black,
          onPressed: (){
            print("Contact is starred");
          }),
    ],
  );
}

Widget buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(width: double.infinity,
            //Height constraint at Container widget level
            height: 400,//250
            child: Image.network(
              "https://i.seadn.io/gae/U_tMhcTTJmyztelSFm6IlmUqJN__hMqWlLUQmp4MOWlM2JLYJNYnvVLTh493Un1Y73tVbW6jv_opFbHFME39smu5mtEjE21eSHi6lDc?auto=format&w=512",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child :Text("Woramet Phatra",//Priyanke Tyagi
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
                data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.orange,
                  )
                ),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.black,
          ),
          emailListTile(),
          Divider(
            color: Colors.black,
          ),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}